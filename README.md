# Projet: Cuisson d'une saucisse

## Table des matieres

* [Modelisation Chaleur en PDF](#modelisation-chaleur-en-pdf)
* [Description du projet](#description-du-projet)
* [Modelisation sur MATLAB](#modelisation-sur-matlab)
* [Code source du rapport](#code-source-du-rapport)

## Modelisation Chaleur en PDF

Il est possible de consulter le rapport en cliquant sur le lien suivant :
[ModPlanCylSphere.pdf](RapportPDF/ModPlanCylSphere.pdf)

Pour le télécharger appuyer sur le nuage.

## Description du projet

Le projet a pour but d'apprendre l'tilisation de la méthode
des éléments finis pour prédire la température
au sein d’une saucisse lors de la cuisson.

Cet approxmation est fait à l'aide de la méthode de Gauss.

## Modelisation sur MATLAB

Voici le lien vers le fichier MATLAB :

* [Model\_Explicite\_Implicite.m](Model_Explicite_Implicite.m)

## Code source du rapport

Le code source du rapport se trouve dans ModTexFiles.
